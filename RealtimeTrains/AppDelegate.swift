//
//  AppDelegate.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/1/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
}
