//
//  ViewController.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/1/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    private let cellReuseId = "TripCell"
    private var foregroundObserver: NSObjectProtocol?
    
    private let tripFinder = TripFinder()
    
    private let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter
    }()
    
    @IBOutlet private weak var swapButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tripFinder.delegate = self
        
        //
        
        foregroundObserver = NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { [weak self] notification in
            self?.tripFinder.findTrips()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tripFinder.findTrips()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(tripFinder.origin.desc) to \(tripFinder.destination.desc)"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tripFinder.tripList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId, for: indexPath) as! TripCell
        
        let trip = tripFinder.tripList[indexPath.row]
        
        cell.populate(trainCode: trip.origin.trainCode,
                      trainOrigin: trip.stationData.origin,
                      trainDestination: trip.stationData.destination,
                      tripOrigin: trip.origin.locationFullName,
                      tripDestination: trip.destination.locationFullName,
                      originArrival: trip.origin.expectedArrival,
                      originDeparture: trip.origin.expectedDeparture,
                      destinationArrival: trip.destination.expectedArrival,
                      timeFormatter: timeFormatter)
        
        return cell
    }
    
    // MARK: - Actions
    
    @IBAction func swapPressed(_ sender: Any) {
        tripFinder.swapDestination()
    }
}


// MARK: -

extension ViewController: TripFinderDelegate {
    func tripFinderDidStartLoadingData() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        swapButton.isEnabled = false
    }
    
    func tripFinderDidReloadData() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        swapButton.isEnabled = true
        tableView.reloadData()
    }
    
    func tripFinderDidFail(withError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        swapButton.isEnabled = true
        
        //
        
        let alert = UIAlertController(title: "Could not load trip data", message: error.localizedDescription, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
}
