//
//  TripHelper.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/5/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import Foundation

class TripHelper {
    static func possibleTrip(_ trainMovementsList: [TrainMovements],
                             originStationCode: String,
                             destinationStationCode: String,
                             stationData: StationData) -> Trip?
    {
        var origin: TrainMovements?
        var destination: TrainMovements?
        
        for movement in trainMovementsList {
            if movement.locationType != .timingPoint {
                if movement.locationCode == originStationCode {
                    origin = movement
                } else if movement.locationCode == destinationStationCode {
                    destination = movement
                }
                
                if origin != nil && destination != nil {
                    break
                }
            }
        }
        
        if let departure = origin?.expectedDeparture, let arrival = destination?.expectedArrival {
            if departure < arrival {
                return Trip(origin: origin!,
                            destination: destination!,
                            stationData: stationData)
            }
        }
        
        return nil
    }
}
