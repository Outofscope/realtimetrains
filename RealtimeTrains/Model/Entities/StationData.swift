//
//  StationData.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/1/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import Foundation

// http://api.irishrail.ie/realtime/
// http://api.irishrail.ie/realtime/realtime.asmx/getStationDataByCodeXML?StationCode=arklw

struct StationData {
    let trainCode: String
    let trainType: String
    let trainDate: Date

    let stationFullName: String
    let stationCode: String
    
    let origin: String
    let destination: String
    
    let originTime: Date
    let destinationTime: Date
    
    let expectedArrival: Date
    let expectedDeparture: Date
    let scheduledArrival: Date
    let sceduledDeparture: Date
    
    let direction: String
    
    // XML field names
    private static let TRAIN_CODE = "Traincode"
    private static let TRAIN_TYPE = "Traintype"
    private static let TRAIN_DATE = "Traindate"
    private static let STATION_FULL_NAME = "Stationfullname"
    private static let STATION_CODE = "Stationcode"
    private static let ORIGIN = "Origin"
    private static let DESTINATION = "Destination"
    private static let ORIGIN_TIME = "Origintime"
    private static let DESTINATION_TIME = "Destinationtime"
    private static let DIRECTION = "Direction"

    static func create(fromDict dict: [String:String]) -> StationData? {
        
        let fields = [TRAIN_CODE,
                      TRAIN_DATE,
                      TRAIN_TYPE,
                      STATION_FULL_NAME,
                      STATION_CODE,
                      ORIGIN,
                      DESTINATION,
                      ORIGIN_TIME,
                      DESTINATION_TIME,
                      DIRECTION]
        
        let absentFields: [String] = fields.compactMap { dict[$0] == nil ? $0 : nil }
        
        if absentFields.count == 0 {
            
            let trainDate = FormatHelper.instance.date(fromDateString: dict[TRAIN_DATE]!)!
            let originTimeInterval = FormatHelper.timeInterval(fromTimeString: dict[ORIGIN_TIME]!)
            let destinationTimeInterval = FormatHelper.timeInterval(fromTimeString: dict[DESTINATION_TIME]!)

            let originTime = trainDate.addingTimeInterval(originTimeInterval)
            
            let destinationTime = FormatHelper.timeInFuture(forTimeInterval: destinationTimeInterval,
                                                            originTimeInterval: originTimeInterval,
                                                            date: trainDate)
            
            return StationData(trainCode: dict[TRAIN_CODE]!,
                               trainType: dict[TRAIN_TYPE]!,
                               trainDate: trainDate,
                               stationFullName: dict[STATION_FULL_NAME]!,
                               stationCode: dict[STATION_CODE]!,
                               origin: dict[ORIGIN]!,
                               destination: dict[DESTINATION]!,
                               originTime: originTime, // assumption: date is same as trainDate
                               destinationTime: destinationTime, // assumption: if less than originTime, then it's next day arrival
                               expectedArrival: Date(),
                               expectedDeparture: Date(),
                               scheduledArrival: Date(),
                               sceduledDeparture: Date(),
                               direction: dict[DIRECTION]!)
        } else {
            return nil
        }
    }
}
