//
//  TrainMovements.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/1/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import Foundation

// http://api.irishrail.ie/realtime/
// http://api.irishrail.ie/realtime/realtime.asmx/getTrainMovementsXML?TrainId=A602&TrainDate=06%20dec%202019

enum TrainLocationType {
    case origin
    case stop
    case timingPoint // non stopping location
    case destination
}

struct TrainMovements {
    let trainCode: String
    let trainDate: Date
    
    let locationFullName: String
    let locationCode: String
    
    let scheduledArrival: Date?
    let scheduledDeparture: Date?
    let expectedArrival: Date?
    let expectedDeparture: Date?
    
    let locationType: TrainLocationType
    let locationOrder: Int
    
    // XML field names
    private static let TRAIN_CODE = "TrainCode"
    private static let TRAIN_DATE = "TrainDate"
    private static let LOCATION_FULL_NAME = "LocationFullName"
    private static let LOCATION_CODE = "LocationCode"
    private static let SCHEDULED_ARRIVAL = "ScheduledArrival"
    private static let SCHEDULED_DEPARTURE = "ScheduledDeparture"
    private static let EXPECTED_ARRIVAL = "ExpectedArrival"
    private static let EXPECTED_DEPARTURE = "ExpectedDeparture"
    private static let LOCATION_TYPE = "LocationType"
    private static let LOCATION_ORDER = "LocationOrder"

    private static let locationTypeMap: [String:TrainLocationType] = ["O": .origin, "S": .stop, "T": .timingPoint, "D": .destination]
    
    static func scheduledDeparture(fromDict dict: [String:String]) -> TimeInterval? {
        if let scheduledDepartureStr = dict[SCHEDULED_DEPARTURE] {
            return FormatHelper.timeInterval(fromTimeString: scheduledDepartureStr)
        } else {
            return nil
        }
    }

    static func create(fromDict dict: [String:String], originDeparture: TimeInterval) -> TrainMovements? {
        
        let fields = [TRAIN_CODE,
                      TRAIN_DATE,
                      LOCATION_FULL_NAME,
                      LOCATION_CODE,
                      SCHEDULED_ARRIVAL,
                      SCHEDULED_DEPARTURE,
                      EXPECTED_ARRIVAL,
                      EXPECTED_DEPARTURE,
                      LOCATION_TYPE,
                      LOCATION_ORDER]
        
        let absentFields: [String] = fields.compactMap { dict[$0] == nil ? $0 : nil }
        
        if absentFields.count == 0 && locationTypeMap.keys.contains(dict[LOCATION_TYPE]!) {
            
            let trainDate = FormatHelper.instance.date(fromDateString: dict[TRAIN_DATE]!)!
            let locationType = locationTypeMap[dict[LOCATION_TYPE]!]!
            let locationOrder = Int(dict[LOCATION_ORDER]!) ?? 0
            
            let scheduledArrival: Date?
            let scheduledDeparture: Date?
            let expectedArrival: Date?
            let expectedDeparture: Date?
            
            if locationType != .origin { // origin doesn't have arrival time
                let expArrTimeInterval = FormatHelper.timeInterval(fromTimeString: dict[EXPECTED_ARRIVAL]!)
                let schArrTimeInterval = FormatHelper.timeInterval(fromTimeString: dict[SCHEDULED_ARRIVAL]!)
                
                scheduledArrival = FormatHelper.timeInFuture(forTimeInterval: schArrTimeInterval,
                                                             originTimeInterval: originDeparture,
                                                             date: trainDate)
                
                expectedArrival = FormatHelper.timeInFuture(forTimeInterval: expArrTimeInterval,
                                                            originTimeInterval: originDeparture,
                                                            date: trainDate)
            } else {
                scheduledArrival = nil
                expectedArrival = nil
            }
            
            if locationType != .destination { // destination doesn't have departure time
                let expDepTimeInterval = FormatHelper.timeInterval(fromTimeString: dict[EXPECTED_DEPARTURE]!)
                let schDepTimeInterval = FormatHelper.timeInterval(fromTimeString: dict[SCHEDULED_DEPARTURE]!)
                
                scheduledDeparture = FormatHelper.timeInFuture(forTimeInterval: schDepTimeInterval,
                                                               originTimeInterval: originDeparture,
                                                               date: trainDate)
                
                expectedDeparture = FormatHelper.timeInFuture(forTimeInterval: expDepTimeInterval,
                                                              originTimeInterval: originDeparture,
                                                              date: trainDate)
            } else {
                scheduledDeparture = nil
                expectedDeparture = nil
            }

            return TrainMovements(trainCode: dict[TRAIN_CODE]!,
                                  trainDate: trainDate,
                                  locationFullName: dict[LOCATION_FULL_NAME]!,
                                  locationCode: dict[LOCATION_CODE]!,
                                  scheduledArrival: scheduledArrival,
                                  scheduledDeparture: scheduledDeparture,
                                  expectedArrival: expectedArrival,
                                  expectedDeparture: expectedDeparture,
                                  locationType: locationType,
                                  locationOrder: locationOrder)
        } else {
            return nil
        }
    }
}
