//
//  Trip.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/5/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import Foundation

struct Trip {
    let origin: TrainMovements
    let destination: TrainMovements
    
    let stationData: StationData
}
