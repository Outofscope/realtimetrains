//
//  Station.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/6/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import Foundation

// http://api.irishrail.ie/realtime/
// http://api.irishrail.ie/realtime/realtime.asmx/getAllStationsXML

struct Station {
    let desc: String
    let code: String
}
