//
//  DateFormatter.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/1/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import Foundation

class FormatHelper {
    static var instance: FormatHelper = {
        return FormatHelper()
    }()
        
    private var dateReadFormatter: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "dd MMM yyyy"
        f.timeZone = TimeZone(identifier: "Europe/Dublin")
        return f
    }()
    
    func date(fromDateString dateString: String, timeString: String?) -> Date? {
        return dateReadFormatter.date(from: dateString)
    }
    
    func date(fromDateString dateString: String) -> Date? {
        return date(fromDateString: dateString, timeString: nil)
    }
    
    //
    
    static func timeInterval(fromTimeString timeString: String) -> TimeInterval {
        let components = timeString.split(separator: ":")
        var interval: TimeInterval = 0
        
        if components.count > 0 { // hours
            interval += (Double(String(components[0])) ?? 0) * 60 * 60
        }
        
        if components.count > 1 { // minutes
            interval += (Double(String(components[1])) ?? 0) * 60
        }
        
        if components.count > 2 { // seconds
            interval += (Double(String(components[2])) ?? 0)
        }
        
        return interval
    }
    
    static func timeInFuture(forTimeInterval timeInterval: TimeInterval,
                             originTimeInterval: TimeInterval,
                             date: Date) -> Date {
        if timeInterval < originTimeInterval { // it's next day
            return date.addingTimeInterval(timeInterval + 24 * 60 * 60)
        } else {
            return date.addingTimeInterval(timeInterval)
        }
    }
}

extension Date {
    func formatForUrl() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Europe/Dublin")
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.string(from: self).lowercased().addingPercentEncoding(withAllowedCharacters: .alphanumerics)
    }
}
