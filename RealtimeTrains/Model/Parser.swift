//
//  Parser.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/1/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import Foundation

class Parser: NSObject {
    
    // XML field names
    private let STATION_DATA = "objStationData"
    private let TRAIN_MOVEMENTS = "objTrainMovements"
    
    private var currentDataFieldName: String?

    private var currentElementName: String?
    private var currentElement: [String:String]?
    private var elementList: [[String:String]]?
    
    func parseStationData(_ data: Data) -> [StationData] {
        currentDataFieldName = STATION_DATA
        
        let xmlParser = XMLParser(data: data)
        xmlParser.delegate = self
        xmlParser.parse()
                
        if let elementList = elementList {
            return elementList.compactMap { StationData.create(fromDict: $0) }
        } else {
            return []
        }
    }
    
    func parseTrainMovements(_ data: Data) -> [TrainMovements] {
        currentDataFieldName = TRAIN_MOVEMENTS

        let xmlParser = XMLParser(data: data)
        xmlParser.delegate = self
        xmlParser.parse()
        
        let result: [TrainMovements]
                
        if let elementList = elementList, elementList.count >= 2 { // at least origin and destination
            let originElement = elementList[0]
            if let originDeparture = TrainMovements.scheduledDeparture(fromDict: originElement) {
                result = elementList.compactMap { TrainMovements.create(fromDict: $0, originDeparture: originDeparture) }
            } else {
                result = []
            }
        } else {
            result = []
        }
        
        return result
    }
}

extension Parser: XMLParserDelegate {
    func parserDidStartDocument(_ parser: XMLParser) {
        elementList = []
    }
    
    func parser(_ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?,
                attributes attributeDict: [String : String] = [:]) {
        
        currentElementName = elementName
                
        if currentElementName == currentDataFieldName {
            currentElement = [:]
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if currentElementName != nil && currentElement != nil {
            if currentElementName != currentDataFieldName { // one of the StationData content fields
                if currentElement![currentElementName!] == nil {
                    currentElement![currentElementName!] = ""
                }
                currentElement![currentElementName!]!.append(contentsOf: string)
            }
        }
    }
    
    func parser(_ parser: XMLParser,
                didEndElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?) {
                
        currentElementName = nil
        
        if elementName == currentDataFieldName {
            elementList!.append(currentElement!)
            currentElement = nil
        } else if currentElement != nil { // one of the StationData content fields
            if let content = currentElement![elementName] {
                currentElement![elementName] = content.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            }
        }
    }
}
