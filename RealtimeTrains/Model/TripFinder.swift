//
//  TripFinder.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/6/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import Foundation

protocol TripFinderDelegate {
    func tripFinderDidStartLoadingData()
    func tripFinderDidReloadData()
    func tripFinderDidFail(withError error: Error)
}

struct TripFinderError: Error {
    let message: String
}

extension TripFinderError: LocalizedError {
    var errorDescription: String? {
        return message
    }
}

// MARK: -

class TripFinder {
    var delegate: TripFinderDelegate?
    
    var origin: Station { privateOrigin }
    var destination: Station { privateDestination }
    var tripList: [Trip] { privateTripList }
    
    private var privateOrigin = Station(desc: "Shankill", code: "SKILL")
    private var privateDestination = Station(desc: "Howth", code: "HOWTH")
    private var privateTripList = [Trip]()
    
    private let stationDataUrlTpl = "http://api.irishrail.ie/realtime/realtime.asmx/getStationDataByCodeXML?StationCode=%@"
    private let trainMovementsUrlTpl = "http://api.irishrail.ie/realtime/realtime.asmx/getTrainMovementsXML?TrainId=%@&TrainDate=%@"
    
    func findTrips() {
        clearListIfNeeded()
        delegate?.tripFinderDidStartLoadingData()
        
        // for thread safety
        let originStationCode = origin.code
        let destinationStationCode = destination.code
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let url = URL(string: String(format: stationDataUrlTpl, origin.code))!
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.delegate?.tripFinderDidFail(withError: error!)
                }
            } else if let data = data {
                let parser = Parser()
                let stationDataList = parser.parseStationData(data)
                
                var tempTripList = [Trip]()
                
                let listQueue = DispatchQueue(label: "ListQueue")
                let group = DispatchGroup()
                
                //
                
                for stationData in stationDataList {
                    group.enter()
                    
                    let urlStr = String(format: self.trainMovementsUrlTpl, stationData.trainCode, Date().formatForUrl()!)
                    let url = URL(string: urlStr)!
                    
                    let task = session.dataTask(with: url) { (data, response, error) in
                        // I ignore errors here.
                        // In case of serious network or server issues
                        // the user will be notified about an error with previous
                        // request for StationData or about an empty list of found trips.
                        if let data = data {
                            let parser = Parser()
                            let trainMovementsList = parser.parseTrainMovements(data)
                            
                            let trip = TripHelper.possibleTrip(trainMovementsList,
                                                               originStationCode: originStationCode,
                                                               destinationStationCode: destinationStationCode,
                                                               stationData: stationData)
                            
                            if let trip = trip {
                                listQueue.sync {
                                    tempTripList.append(trip)
                                }
                            }
                        }
                        group.leave()
                    }
                    task.resume()
                }
                
                //

                group.notify(queue: listQueue) {
                    
                    tempTripList = tempTripList.sorted(by: { (trip1, trip2) -> Bool in
                        // origin always has departure
                        return trip1.origin.expectedDeparture! < trip2.origin.expectedDeparture!
                    })
                    
                    DispatchQueue.main.async {
                        if tempTripList.count > 0 {
                            self.privateTripList = tempTripList
                            self.delegate?.tripFinderDidReloadData()
                        } else {
                            let error = TripFinderError(message: "No suitable trains found. Please, try again later.")
                            self.delegate?.tripFinderDidFail(withError: error)
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    func swapDestination() {
        let temp = privateOrigin
        privateOrigin = destination
        privateDestination = temp
        
        clearListIfNeeded()
        findTrips()
    }
        
    private func clearListIfNeeded()
    {
        if tripList.count > 0 {
            privateTripList = []
            delegate?.tripFinderDidReloadData()
        }
    }
}
