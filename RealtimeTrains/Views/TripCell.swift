//
//  TripCell.swift
//  RealtimeTrains
//
//  Created by Konstantin Medvedenko on 12/6/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import UIKit

class TripCell: UITableViewCell {
    @IBOutlet private weak var trainCodeLabel: UILabel!
    @IBOutlet private weak var originLabel: UILabel!
    @IBOutlet private weak var destinationLabel: UILabel!
    
    @IBOutlet private weak var originTimingLabel: UILabel!
    @IBOutlet private weak var destinationTimingLabel: UILabel!
    
    private let lightAttrs = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]

    func populate(trainCode: String,
                  trainOrigin: String,
                  trainDestination: String,
                  tripOrigin: String,
                  tripDestination: String,
                  originArrival: Date?,
                  originDeparture: Date?,
                  destinationArrival: Date?,
                  timeFormatter: DateFormatter)
    {
        let trainInfo = NSMutableAttributedString()
        trainInfo.append(NSAttributedString(string: "Train ",
                                            attributes: lightAttrs))
        trainInfo.append(NSAttributedString(string: trainCode))
        trainInfo.append(NSAttributedString(string: " \(trainOrigin) — \(trainDestination)", attributes: lightAttrs))

        trainCodeLabel.attributedText = trainInfo
        
        //
        
        originLabel.text = tripOrigin
        destinationLabel.text = tripDestination
        
        //
        
        let originTiming = NSMutableAttributedString()
        if let arrival = originArrival {
            originTiming.append(NSAttributedString(string: "Arr ", attributes: lightAttrs))
            originTiming.append(NSAttributedString(string: timeFormatter.string(from: arrival)))
            originTiming.append(NSAttributedString(string: " "))
        }
        
        if let departure = originDeparture {
            originTiming.append(NSAttributedString(string: "Dpt ", attributes: lightAttrs))
            originTiming.append(NSAttributedString(string: timeFormatter.string(from: departure)))
        }
        
        originTimingLabel.attributedText = originTiming
        
        //
        
        let destinationTiming = NSMutableAttributedString()
        if let arrival = destinationArrival {
            destinationTiming.append(NSAttributedString(string: "Arr ", attributes: lightAttrs))
            destinationTiming.append(NSAttributedString(string: timeFormatter.string(from: arrival)))
        }
        
        destinationTimingLabel.attributedText = destinationTiming
    }
}
