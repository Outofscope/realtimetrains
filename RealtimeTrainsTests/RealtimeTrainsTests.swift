//
//  RealtimeTrainsTests.swift
//  RealtimeTrainsTests
//
//  Created by Konstantin Medvedenko on 12/1/19.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import XCTest
@testable import RealtimeTrains

class RealtimeTrainsTests: XCTestCase {
        
    func testStationDataParsing() {
        continueAfterFailure = false
        
        let parser = Parser()
        
        let data = loadData(fromXmlFile: "StationData-Arklow-001")
        let stationDataList = parser.parseStationData(data)
        
        XCTAssertEqual(stationDataList.count, 2)
        
        let stationData1 = stationDataList[0]
        XCTAssertEqual(stationData1.trainCode, "A601")
        XCTAssertEqual(stationData1.trainType, "ICR")
        XCTAssertEqual(stationData1.trainDate.testStr(), "2019-12-01 00:00:00")
        XCTAssertEqual(stationData1.stationFullName, "Arklow")
        XCTAssertEqual(stationData1.stationCode, "ARKLW")
        XCTAssertEqual(stationData1.origin, "Rosslare Europort")
        XCTAssertEqual(stationData1.destination, "Dublin Connolly")
        XCTAssertEqual(stationData1.originTime.testStr(), "2019-12-01 09:40:00")
        XCTAssertEqual(stationData1.destinationTime.testStr(), "2019-12-01 12:29:00")
        XCTAssertEqual(stationData1.direction, "Northbound")

        let stationData2 = stationDataList[1]
        XCTAssertEqual(stationData2.trainCode, "A600")
        XCTAssertEqual(stationData2.trainType, "ICR")
        XCTAssertEqual(stationData2.trainDate.testStr(), "2020-05-12 00:00:00")
        XCTAssertEqual(stationData2.stationFullName, "Arklow")
        XCTAssertEqual(stationData2.stationCode, "ARKLW")
        XCTAssertEqual(stationData2.origin, "Dublin Connolly")
        XCTAssertEqual(stationData2.destination, "Rosslare Europort")
        XCTAssertEqual(stationData2.originTime.testStr(), "2020-05-12 23:25:00")
        XCTAssertEqual(stationData2.destinationTime.testStr(), "2020-05-13 02:17:00")
        XCTAssertEqual(stationData2.direction, "Southbound")
    }
    
    func testTrainMovementsParsing() {
        continueAfterFailure = false
        
        let parser = Parser()
        
        let stationDataList = parser.parseTrainMovements(loadData(fromXmlFile: "TrainMovements-E201-001"))
        let stationDataList2 = parser.parseTrainMovements(loadData(fromXmlFile: "TrainMovements-A609-001"))

        XCTAssertEqual(stationDataList.count, 27)
        XCTAssertEqual(stationDataList2.count, 29)

        let trainMovement1 = stationDataList[0]
        XCTAssertEqual(trainMovement1.trainCode, "E201")
        XCTAssertEqual(trainMovement1.trainDate.testStr(), "2019-12-01 00:00:00")
        XCTAssertEqual(trainMovement1.locationFullName, "Howth")
        XCTAssertEqual(trainMovement1.locationCode, "HOWTH")
        XCTAssertEqual(trainMovement1.locationType, .origin)
        XCTAssertNil(trainMovement1.scheduledArrival)
        XCTAssertEqual(trainMovement1.scheduledDeparture!.testStr(), "2019-12-01 10:05:17")
        XCTAssertNil(trainMovement1.expectedArrival)
        XCTAssertEqual(trainMovement1.expectedDeparture!.testStr(), "2019-12-01 10:07:01")

        let trainMovement2 = stationDataList[1]
        XCTAssertEqual(trainMovement2.locationType, .stop)
        XCTAssertEqual(trainMovement2.scheduledArrival!.testStr(), "2019-12-01 10:08:00")
        XCTAssertEqual(trainMovement2.scheduledDeparture!.testStr(), "2019-12-01 10:08:30")
        XCTAssertEqual(trainMovement2.expectedArrival!.testStr(), "2019-12-01 10:09:24")
        XCTAssertEqual(trainMovement2.expectedDeparture!.testStr(), "2019-12-01 10:09:18") // real data from real API, they expect to depart before arrival
        
        let trainMovement3 = stationDataList[26]
        XCTAssertEqual(trainMovement3.locationType, .destination)
        XCTAssertEqual(trainMovement3.scheduledArrival!.testStr(), "2019-12-02 01:12:00")
        XCTAssertNil(trainMovement3.scheduledDeparture)
        XCTAssertEqual(trainMovement3.expectedArrival!.testStr(), "2019-12-02 01:19:54")
        XCTAssertNil(trainMovement3.expectedDeparture)
        
        XCTAssertEqual(stationDataList2[13].locationType, .timingPoint)
    }
    
    func testTimeStringParsing() {
        XCTAssertEqual(FormatHelper.timeInterval(fromTimeString: "00:00:00"), 0)
        XCTAssertEqual(FormatHelper.timeInterval(fromTimeString: "00:01:15"), 75)
        XCTAssertEqual(FormatHelper.timeInterval(fromTimeString: "03:22:59"), 12179)
        XCTAssertEqual(FormatHelper.timeInterval(fromTimeString: "00:00"), 0)
        XCTAssertEqual(FormatHelper.timeInterval(fromTimeString: "00:45"), 2700)
        XCTAssertEqual(FormatHelper.timeInterval(fromTimeString: "01:17"), 4620)
    }
    
    func testDateFormatting() {
        XCTAssertEqual(Date.date(fromTestStr: "2019-12-06 00:00:00")!.formatForUrl(), "06%20dec%202019")
        XCTAssertEqual(Date.date(fromTestStr: "2015-07-22 00:00:00")!.formatForUrl(), "22%20jul%202015")
        XCTAssertEqual(Date.date(fromTestStr: "2020-10-31 00:00:00")!.formatForUrl(), "31%20oct%202020")
        XCTAssertEqual(Date.date(fromTestStr: "2033-01-01 00:00:00")!.formatForUrl(), "01%20jan%202033")
    }

    func testTrainSuitability() {
        continueAfterFailure = false

        let parser = Parser()

        let movements1 = parser.parseTrainMovements(loadData(fromXmlFile: "TrainMovements-E201-001"))
        let movements2 = parser.parseTrainMovements(loadData(fromXmlFile: "TrainMovements-A609-001"))
        
        XCTAssertNil(TripHelper.possibleTrip(movements1, originStationCode: "ARKLW", destinationStationCode: "SKILL"))
        XCTAssertNil(TripHelper.possibleTrip(movements1, originStationCode: "SKILL", destinationStationCode: "ARKLW"))
        XCTAssertNotNil(TripHelper.possibleTrip(movements1, originStationCode: "KBRCK", destinationStationCode: "SEAPT"))
        XCTAssertNil(TripHelper.possibleTrip(movements1, originStationCode: "SEAPT", destinationStationCode: "KBRCK"))

        XCTAssertNil(TripHelper.possibleTrip(movements2, originStationCode: "ARKLW", destinationStationCode: "SKILL"))
        XCTAssertNil(TripHelper.possibleTrip(movements2, originStationCode: "SKILL", destinationStationCode: "ARKLW"))
        XCTAssertNil(TripHelper.possibleTrip(movements2, originStationCode: "KCOOL", destinationStationCode: "CNLLY"))
        XCTAssertNil(TripHelper.possibleTrip(movements2, originStationCode: "CNLLY", destinationStationCode: "KCOOL"))
        XCTAssertNotNil(TripHelper.possibleTrip(movements2, originStationCode: "RLEPT", destinationStationCode: "DLERY"))
        XCTAssertNotNil(TripHelper.possibleTrip(movements2, originStationCode: "RLEPT", destinationStationCode: "CNLLY"))
    }
    
    //
    
    func loadData(fromXmlFile xmlFileName: String) -> Data {
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: xmlFileName, ofType: "xml")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        return data
    }
}

extension Date {
    static func date(fromTestStr testStr: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Europe/Dublin")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: testStr)
    }
    
    func testStr() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Europe/Dublin")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: self)
    }
}
